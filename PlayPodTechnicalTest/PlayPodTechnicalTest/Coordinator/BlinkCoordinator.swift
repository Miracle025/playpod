//
//  BlinkCoordinator.swift
//  PlayPodTechnicalTest
//
//  Created by Fateme on 7/25/1399 AP.
//  Copyright © 1399 AP Fateme. All rights reserved.
//

import UIKit
class BlinkCoordinator: Coordinator {
    var childCoordinators: [Coordinator]
    var navigationController: UINavigationController
    init(navigationController: UINavigationController) {
        self.navigationController = navigationController
        self.childCoordinators = []
    }
    func start() {
        let vc = BlinkViewController()
        let tabBarItem = UITabBarItem(title: "", image: UIImage(named: "blink"), selectedImage: UIImage(named: "blink.selected"))
        vc.tabBarItem = tabBarItem
        navigationController.setNavigationBarHidden(true, animated: true)
        navigationController.pushViewController(vc, animated: true)
    }
    func finish() { }
}
