//
//  NavigationRouter.swift
//  PlayPodTechnicalTest
//
//  Created by Fateme on 7/25/1399 AP.
//  Copyright © 1399 AP Fateme. All rights reserved.
//

import UIKit
public class NavigationRouter: NSObject {
    private let navigationController: UINavigationController
    private let routerRootViewController: UIViewController?
    private var onDismissForViewController: [UIViewController: () -> Void] = [:]
    public init(navigationController: UINavigationController) {
        self.navigationController = navigationController
        self.routerRootViewController = navigationController.viewControllers.first
        super.init()
        navigationController.delegate = self
    }
}
extension NavigationRouter: Router {
    func present(_ viewController: UIViewController, animated: Bool, onDismissed: (() -> Void)?) {
        onDismissForViewController[viewController] = onDismissed
        navigationController.pushViewController(viewController, animated: animated)
    }
    func dismiss(animated: Bool) {
        guard let routerRootViewController = routerRootViewController else {
            navigationController.popToRootViewController(animated: animated)
            return
        }
        performDimissed(for: routerRootViewController)
        navigationController.popToViewController(routerRootViewController, animated: animated)
    }
    private func performDimissed(for viewController: UIViewController) {
        guard let onDismiss = onDismissForViewController[viewController] else { return }
        onDismiss()
        onDismissForViewController[viewController] = nil
    }
}
extension NavigationRouter: UINavigationControllerDelegate {
    public func navigationController(_ navigationController: UINavigationController, didShow viewController: UIViewController, animated: Bool) {
        guard let dismissViewController = navigationController.transitionCoordinator?.viewController(forKey: .from), !navigationController.viewControllers.contains(dismissViewController) else { return }
        performDimissed(for: dismissViewController)
    }
}
