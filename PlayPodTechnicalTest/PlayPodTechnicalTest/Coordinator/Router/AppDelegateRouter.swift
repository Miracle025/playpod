//
//  AppDelegateRouter.swift
//  PlayPodTechnicalTest
//
//  Created by Fateme on 7/25/1399 AP.
//  Copyright © 1399 AP Fateme. All rights reserved.
//

import UIKit
class AppDelegateRouter {
    var window: UIWindow?
    init(_ window: UIWindow?) {
        self.window = window
    }
    @discardableResult
    func getCoordinator() -> Coordinator {
        let coordinator = MainTabBarCoordinator(navigationController: CustomNavigationController())
        setRootViewController(coordinator.navigationController)
        return coordinator
    }
    func setRootViewController(_ viewController: UIViewController) {
        window?.rootViewController?.dismiss(animated: true, completion: nil)
        window?.rootViewController = viewController
        window?.makeKeyAndVisible()
    }
    @available(iOS 13, *)
    private func setrrotViewController(_ viewController: UIViewController, windowScene: UIWindowScene) {
        window?.windowScene = windowScene
        window?.rootViewController = viewController
        window?.makeKeyAndVisible()
    }
}
