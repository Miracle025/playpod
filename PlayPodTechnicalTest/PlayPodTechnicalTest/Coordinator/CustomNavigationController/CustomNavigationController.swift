//
//  CustomNavigationController.swift
//  PlayPodTechnicalTest
//
//  Created by Fateme on 7/26/1399 AP.
//  Copyright © 1399 AP Fateme. All rights reserved.
//
import UIKit
class CustomNavigationController: UINavigationController {
    override func viewDidLoad() {
        super.viewDidLoad()
        configureView()
    }
    private func configureView() {
//        let textAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white, NSAttributedString.Key.font: UIFont()]
//        navigationBar.titleTextAttributes = textAttributes
        navigationBar.tintColor = .white//GlobalSettings.shared().darkBlue
        navigationBar.barTintColor = .white//GlobalSettings.shared().blue3
        navigationBar.shadowImage = UIImage()
        view.backgroundColor = .white//GlobalSettings.shared().blue3
    }
}
