//
//  HomeCoordiantor.swift
//  PlayPodTechnicalTest
//
//  Created by Fateme on 7/25/1399 AP.
//  Copyright © 1399 AP Fateme. All rights reserved.
//

import UIKit
class HomeCoordiantor: Coordinator {
    var childCoordinators: [Coordinator]
    var navigationController: UINavigationController
    init(navigationController: UINavigationController) {
        self.navigationController = navigationController
        childCoordinators = []
    }
    func start() {
        let vc = HomeViewController(viewModel: HomeViewModel(interactor: Interactor<Food>()))
        let tabBarItem = UITabBarItem(title: "", image: UIImage(named: "home"), selectedImage: UIImage(named: "home.selected"))
        vc.tabBarItem = tabBarItem
        vc.delegate = self
        navigationController.setNavigationBarHidden(true, animated: true)
        navigationController.pushViewController(vc, animated: true)
    }
    func finish() { }
}
extension HomeCoordiantor: HomeViewControllerDelegate {
    func didTapOnCell(with item: Food) {
        let vc = ShowDetailsFoodontroller(viewModel: ShowDetailFoodViewModel(interactor: Interactor<DetailFoodModel>()))
        navigationController.pushViewController(vc, animated: true)
    }
}
