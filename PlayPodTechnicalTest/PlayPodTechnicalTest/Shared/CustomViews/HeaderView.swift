//
//  HeaderView.swift
//  PlayPodTechnicalTest
//
//  Created by Fateme on 7/26/1399 AP.
//  Copyright © 1399 AP Fateme. All rights reserved.
//
import UIKit
class HeaderView: UIView {
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
    override func draw(_ rect: CGRect) {
        guard let context = UIGraphicsGetCurrentContext() else {
            return
        }
        let colorSpace = CGColorSpaceCreateDeviceRGB()
        drawView(in: rect, in: context, with: colorSpace)
    }
    func drawView(in rect: CGRect, in context: CGContext, with colorSpace: CGColorSpace?) {
        let widthView = self.bounds.width
        let heightView = self.bounds.height
        
        let path = UIBezierPath()
        path.move(to: CGPoint(x: 0, y: 0))
        path.addLine(to: CGPoint(x: widthView, y: 0))
        path.addLine(to: CGPoint(x: widthView, y: heightView-20))
        path.addCurve(to: CGPoint(x: widthView/2, y: heightView-20), controlPoint1: CGPoint(x: widthView*5/6, y: heightView), controlPoint2: CGPoint(x: widthView*4/6, y: heightView-40))
        path.addCurve(to: CGPoint(x: 0, y: heightView-20), controlPoint1: CGPoint(x: widthView*2/6, y: heightView), controlPoint2: CGPoint(x: widthView/6, y: heightView-40))
        
        let colors = [Colors.sharedInstance.brown, Colors.sharedInstance.lightBrown]
        let locatoins: [NSNumber] = [0, 1]
        let shapeLayer = CAShapeLayer()
        shapeLayer.path = path.cgPath
        self.layer.mask = shapeLayer
        applyGradient(colours: colors, locations: locatoins, corner: 0)
    }
}
