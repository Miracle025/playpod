//
//  CustomButton.swift
//  PlayPodTechnicalTest
//
//  Created by Fateme on 7/26/1399 AP.
//  Copyright © 1399 AP Fateme. All rights reserved.
//

import UIKit
class CustomButton: UIView {
    let gradientLayer = CAGradientLayer()
    let button = UIButton()
    override init(frame: CGRect) {
        super.init(frame: frame)
        configuration()
    }
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        configuration()
    }
    func configuration() {
        setShadow()
        setGradient()
        configurationButton()
    }
    func setGradient() {
        gradientLayer.locations = [0.0, 1.0]
        gradientLayer.colors = [Colors.sharedInstance.mediumBrown.cgColor, Colors.sharedInstance.lightBrown.cgColor]
        gradientLayer.startPoint = CGPoint(x: 0.0, y: 0.0)
        gradientLayer.endPoint = CGPoint(x: 1.0, y: 1.0)
        gradientLayer.frame = self.bounds
        gradientLayer.cornerRadius = 20
        self.layer.insertSublayer(gradientLayer, at: 0)
    }
    func setShadow() {
        self.layer.shadowColor = Colors.sharedInstance.lightBrown.cgColor
        self.layer.shadowOffset = CGSize(width: 0, height: 1)
        self.layer.shadowOpacity = 1
        self.layer.shadowRadius = 1.0
        self.clipsToBounds = false
        self.layer.masksToBounds = false
    }
    func configurationButton() {
        self.addSubview(button)
        button.frame = CGRect(x: 0, y: 0, width: self.frame.width, height: self.frame.height)
        button.layer.cornerRadius = 20
        button.setTitle("Add To Card", for: .normal)
        button.backgroundColor = .clear
        button.setTitleColor(.white, for: .normal)
    }
    override func layoutSubviews() {
        gradientLayer.frame = self.bounds
        button.frame = CGRect(x: 0, y: 0, width: self.frame.width, height: self.frame.height)
    }
}
