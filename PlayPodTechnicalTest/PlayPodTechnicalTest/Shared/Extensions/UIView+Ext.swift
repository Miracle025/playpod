//
//  UIView+Ext.swift
//  PlayPodTechnicalTest
//
//  Created by Fateme on 7/26/1399 AP.
//  Copyright © 1399 AP Fateme. All rights reserved.
//

import UIKit
extension UIView {
    @discardableResult
    func applyGradient(colours: [UIColor], locations: [NSNumber]?, corner: CGFloat) -> CAGradientLayer {
        let gradient: CAGradientLayer = CAGradientLayer()
        gradient.transform = CATransform3DMakeRotation(.pi / 2, 0, 0, 1)
        gradient.frame = self.bounds
        gradient.colors = colours.map { $0.cgColor }
        gradient.locations = locations
        gradient.cornerRadius = corner
        self.layer.insertSublayer(gradient, at: 0)
        return gradient
    }
    @discardableResult
    func applyVerticalGradient(colours: [UIColor], locations: [NSNumber]?, corner: CGFloat) -> CAGradientLayer {
        let gradient: CAGradientLayer = CAGradientLayer()
        gradient.frame = self.bounds
        gradient.colors = colours.map { $0.cgColor }
        gradient.locations = locations
        gradient.cornerRadius = corner
        self.layer.insertSublayer(gradient, at: 0)
        return gradient
    }
}

