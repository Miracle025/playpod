//
//  Colors.swift
//  PlayPodTechnicalTest
//
//  Created by Fateme on 7/26/1399 AP.
//  Copyright © 1399 AP Fateme. All rights reserved.
//

import Foundation
import UIKit
final class Colors: NSObject {
    static let sharedInstance = Colors()
    let lightBrown = UIColor(displayP3Red: 228/255, green: 199/255, blue: 169/255, alpha: 1)
    let mediumBrown = UIColor(displayP3Red: 209/255, green: 160/255, blue: 110/255, alpha: 1)
    let brown = UIColor(displayP3Red: 168/255, green: 111/255, blue: 53/255, alpha: 1)
    let darkBrown = UIColor(displayP3Red: 80/255, green: 53/255, blue: 26/255, alpha: 1)
}
