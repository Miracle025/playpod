//
//  Interactor.swift
//  PlayPodTechnicalTest
//
//  Created by Fateme on 7/25/1399 AP.
//  Copyright © 1399 AP Fateme. All rights reserved.
//

import Foundation
class Interactor<Model: Codable>: Parsable {
    typealias Object = Model
    func parse(data: Data) -> Model? {
        let decoder = JSONDecoder()
        decoder.keyDecodingStrategy = .convertFromSnakeCase
        let model = try? decoder.decode(Model.self, from: data)
        return model
    }
}
