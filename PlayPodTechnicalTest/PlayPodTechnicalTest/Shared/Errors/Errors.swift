//
//  Errors.swift
//  PlayPodTechnicalTest
//
//  Created by Fateme on 7/25/1399 AP.
//  Copyright © 1399 AP Fateme. All rights reserved.
//

import Foundation
enum Errors: String, Error {
    case errorOnLoading = "An error occured while loading posts."
    case errorOnViewInitializing = "An error occured while initializing view controllers."
}
