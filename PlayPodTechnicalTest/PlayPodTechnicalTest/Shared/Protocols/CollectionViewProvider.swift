//
//  CollectionViewProvider.swift
//  PlayPodTechnicalTest
//
//  Created by Fateme on 7/25/1399 AP.
//  Copyright © 1399 AP Fateme. All rights reserved.
//
import UIKit.UICollectionView
protocol CollectionViewProvider: class {
    var collectionView: UICollectionView { get }
    var dataSourceHandler: CollectionViewDataSourceHandler! { get set }
    var delegateHandler:  CollectionViewDelegateHandler! { get set }
    init(collectionView: UICollectionView)
    func registerSections(_ sections: [Sectionable])
    func loadItems(sections: [Sectionable], delegate: UICollectionViewDelegate?)
    func reloadItems(at indexPath: [IndexPath])
    func reloadSections(sections: IndexSet)
}
extension CollectionViewProvider {
    func registerViews(section: Sectionable) {
        if let headerView = section.getHeaderView() {
            collectionView.register(headerView.getViewClass(), forSupplementaryViewOfKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier: headerView.reuseID)
        } else if let footerView = section.getFooterView() {
            collectionView.register(footerView.getViewClass(), forSupplementaryViewOfKind: UICollectionView.elementKindSectionFooter, withReuseIdentifier: footerView.reuseID)
        }
    }
    func loadItems(sections: [Sectionable], delegate: UICollectionViewDelegate?) {
        registerSections(sections)
        dataSourceHandler = CollectionViewDataSourceHandler(sections: sections)
        collectionView.dataSource = dataSourceHandler
        if let delegate = delegate {
            collectionView.delegate = delegate
        }else {
            delegateHandler = CollectionViewDelegateHandler(sections: sections)
            collectionView.delegate = delegateHandler
        }
        collectionView.reloadData()
    }
    func registerSections(_ sections: [Sectionable]) {
        sections.forEach { (section) in
            section.getCells().forEach { (reusable) in
                let nibCell = UINib(nibName: reusable.getReusIdentifier(), bundle: nil)
                collectionView.register(nibCell, forCellWithReuseIdentifier: reusable.getReusIdentifier())
            }
            registerViews(section: section)
        }
    }
    func reloadItems(at indexPath: [IndexPath]) {
        collectionView.reloadItems(at: indexPath)
    }
    func reloadSections(sections: IndexSet) {
        collectionView.reloadSections(sections)
    }
}
