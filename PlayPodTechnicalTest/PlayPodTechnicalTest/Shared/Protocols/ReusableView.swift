//
//  ReusableView.swift
//  PlayPodTechnicalTest
//
//  Created by Fateme on 7/25/1399 AP.
//  Copyright © 1399 AP Fateme. All rights reserved.
//

import Foundation
protocol ReusableView {
    var reuseID: String {get}
    var model: Any? {get set}
    func getViewClass() -> AnyClass
}
