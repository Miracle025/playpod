//
//  Reusable.swift
//  PlayPodTechnicalTest
//
//  Created by Fateme on 7/25/1399 AP.
//  Copyright © 1399 AP Fateme. All rights reserved.
//

import Foundation
protocol Reusable {
    init(reuseIdentifier: String, cellClass: AnyClass, model: Any)
    func getReusIdentifier() -> String
    func getCellClass() -> AnyClass
    func getModel() -> Any
}
