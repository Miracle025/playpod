//
//  Sectionable.swift
//  PlayPodTechnicalTest
//
//  Created by Fateme on 7/25/1399 AP.
//  Copyright © 1399 AP Fateme. All rights reserved.
//

import Foundation
protocol Sectionable: class {
    init(cells: [Reusable], headerView: ReusableView?, footerView: ReusableView?)
    func getCells() -> [Reusable]
    func getFooterView() -> ReusableView?
    func getHeaderView() -> ReusableView?
}
