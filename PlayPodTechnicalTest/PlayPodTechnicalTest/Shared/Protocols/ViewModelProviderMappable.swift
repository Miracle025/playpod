//
//  ViewModelProviderMappable.swift
//  PlayPodTechnicalTest
//
//  Created by Fateme on 7/25/1399 AP.
//  Copyright © 1399 AP Fateme. All rights reserved.
//

import Foundation
protocol ViewModelProviderMappable: ViewModelProvider {
    associatedtype Model: Codable
    var interactor: Interactor<Model> {get}
    init(interactor: Interactor<Model>)
}
