//
//  Viewable.swift
//  PlayPodTechnicalTest
//
//  Created by Fateme on 7/25/1399 AP.
//  Copyright © 1399 AP Fateme. All rights reserved.
//

import Foundation
protocol Viewable: class {
    func show(result: Result<Any, Errors>)
}
