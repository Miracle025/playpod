//
//  SectionProvider.swift
//  PlayPodTechnicalTest
//
//  Created by Fateme on 7/25/1399 AP.
//  Copyright © 1399 AP Fateme. All rights reserved.
//

import Foundation
class SectionProvider: Sectionable {
    // MARK: - Properties
    var cells: [Reusable]
    var footerView: ReusableView?
    var headerView: ReusableView?
    // MARK: - Initialized
    required init(cells: [Reusable], headerView: ReusableView?, footerView: ReusableView?) {
        self.cells = cells
        self.footerView = footerView
        self.headerView = headerView
    }
    // MARK: - Get Properties
    func getCells() -> [Reusable] { return cells }
    func getFooterView() -> ReusableView? { return footerView }
    func getHeaderView() -> ReusableView? { return headerView }
}
