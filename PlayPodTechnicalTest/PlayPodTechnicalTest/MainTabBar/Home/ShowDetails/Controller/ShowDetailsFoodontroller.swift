//
//  ShowDetailsFoodontroller.swift
//  PlayPodTechnicalTest
//
//  Created by Fateme on 7/26/1399 AP.
//  Copyright © 1399 AP Fateme. All rights reserved.
//

import UIKit
class ShowDetailsFoodontroller: UIViewController {
    // MARK: - Views
    @IBOutlet weak var headerView: UIView!
    var holderCollectionView: BaseCollectionView!
    lazy var collectionView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        let width = (UIApplication.shared.windows.first?.bounds.width ?? 300) - 50
        layout.itemSize = CGSize(width: width, height: 60)
        layout.minimumLineSpacing = 10
        layout.scrollDirection = .vertical
        layout.sectionInset = UIEdgeInsets(top: 20, left: 20, bottom: 20, right: 20)
        let collection = UICollectionView(frame: .zero, collectionViewLayout: layout)
        collection.backgroundColor = .white
        return collection
    }()
    let addButton: CustomButton = {
        let button = CustomButton()
        button.layer.cornerRadius = 20
        button.backgroundColor = Colors.sharedInstance.brown
        return button
    }()
    // MARK: - Properties
    var viewModel: ShowDetailFoodViewModel
    // MARK: - Initializer
    init(viewModel: ShowDetailFoodViewModel) {
        self.viewModel = viewModel
        super.init(nibName: nil, bundle: nil)
    }
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    // MARK: - Cycle View
    override func viewDidLoad() {
        super.viewDidLoad()
        layoutView()
        viewModel.set(self)
        viewModel.viewDidLoad()
    }
    //MARK:- View Configuration
    func layoutView() {
        addCollectionView()
        configurationAddButton()
    }
    func addCollectionView() {
        holderCollectionView = BaseCollectionView(collectionView: collectionView)
        holderCollectionView.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(holderCollectionView)
        NSLayoutConstraint.activate([
            holderCollectionView.topAnchor.constraint(equalTo: headerView.bottomAnchor),
            holderCollectionView.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 20),
            holderCollectionView.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -20),
            holderCollectionView.bottomAnchor.constraint(equalTo: view.bottomAnchor)])
    }
    func configurationAddButton() {
        let width =  UIScreen.main.bounds.width
        let tabbarHeight = MainTabBarController().tabBar.frame.height + 10
        addButton.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(addButton)
        if #available(iOS 11.0, *) {
            NSLayoutConstraint.activate([addButton.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor, constant: -10),
                                         addButton.widthAnchor.constraint(equalToConstant: width - 40),
                                         addButton.heightAnchor.constraint(equalToConstant: 40),
                                         addButton.centerXAnchor.constraint(equalTo: view.centerXAnchor)])
        } else {
            NSLayoutConstraint.activate([addButton.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: -tabbarHeight),
                                         addButton.widthAnchor.constraint(equalToConstant: width - 40),
                                         addButton.heightAnchor.constraint(equalToConstant: 40),
                                         addButton.centerXAnchor.constraint(equalTo: view.centerXAnchor)])

        }
    }
}
extension ShowDetailsFoodontroller: Viewable {
    func show(result: Result<Any, Errors>) {
        switch result {
        case .success(let reusableCells):
            guard let sections = reusableCells as? [Sectionable] else { return }
            configureCollectionView(sections)
        case .failure:
            //TODO:Show error view
            break
        }
    }
    func configureCollectionView(_ sections: [Sectionable]){
        holderCollectionView.loadItems(sections: sections, delegate: nil)
    }
}
