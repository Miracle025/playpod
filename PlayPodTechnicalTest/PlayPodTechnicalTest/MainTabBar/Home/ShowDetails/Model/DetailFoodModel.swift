//
//  DetailFoodModel.swift
//  PlayPodTechnicalTest
//
//  Created by Fateme on 7/26/1399 AP.
//  Copyright © 1399 AP Fateme. All rights reserved.
//
struct DetailFoodModel: Codable {
    let title: String
    let emptyImage: String
    let fullImage: String
}
