//
//  ShowDetailFoodViewModel.swift
//  PlayPodTechnicalTest
//
//  Created by Fateme on 7/26/1399 AP.
//  Copyright © 1399 AP Fateme. All rights reserved.
//

import Foundation
class ShowDetailFoodViewModel: ViewModelProvider {
    typealias Model = DetailFoodModel
    var interactor: Interactor<DetailFoodModel>
    weak var view: Viewable?
    var sections = [Sectionable]()
    var detailFood = [DetailFoodModel]()
    required init(interactor: Interactor<DetailFoodModel>) {
        self.interactor = interactor
    }
    func set(_ view: Viewable) {
        self.view = view
    }
    func viewDidLoad() {
        loadItems()
    }
    func getItems() {
        //TODO:Add mock data
        detailFood = [DetailFoodModel(title: "Temperature", emptyImage: "temperture", fullImage: "tempereture.full"),
                      DetailFoodModel(title: "Sugar", emptyImage: "sugar", fullImage: "sugare.fill"),
                      DetailFoodModel(title: "Milk", emptyImage: "milk", fullImage: "milk.full")]
    }
    func loadItems() {
        getItems()
        prepareCells(detailFood)
        self.view?.show(result: .success(sections))
    }
    func prepareCells(_ items: [DetailFoodModel]) {
        let cells = items.map({ CellViewModel(reuseIdentifier: ShowDetailFoodCell.reuseID, cellClass: ShowDetailFoodCell.self, model: $0)})
        sections = [SectionProvider(cells: cells, headerView: nil, footerView: nil)]
    }
}
