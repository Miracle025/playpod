//
//  ShowDetailFoodCell.swift
//  PlayPodTechnicalTest
//
//  Created by Fateme on 7/26/1399 AP.
//  Copyright © 1399 AP Fateme. All rights reserved.
//

import UIKit

class ShowDetailFoodCell: UICollectionViewCell {
    // MARK: Views
    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var fullImage: UIImageView!
    @IBOutlet weak var emptyimage: UIImageView!
    // MARK: - Properties
    static let reuseID: String = {
        return String(describing: ShowDetailFoodCell.self)
    }()
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }
}
extension ShowDetailFoodCell:Updatable {
    func update(model: Any) {
        guard let model = model as? DetailFoodModel else { return }
        title.text = model.title
        fullImage.image = UIImage(named: model.fullImage)
        emptyimage.image = UIImage(named: model.emptyImage)
    }
}
