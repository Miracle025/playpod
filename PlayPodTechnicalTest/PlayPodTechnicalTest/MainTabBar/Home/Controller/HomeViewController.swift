//
//  HomeViewController.swift
//  PlayPodTechnicalTest
//
//  Created by Fateme on 7/25/1399 AP.
//  Copyright © 1399 AP Fateme. All rights reserved.
//

import UIKit
protocol HomeViewControllerDelegate: class {
    func didTapOnCell(with item: Food)
}
class HomeViewController: UIViewController {
    // MARK: - Variabels
    var delegate: HomeViewControllerDelegate?
    let viewModel: HomeViewModel
    var holderCollectionView: BaseCollectionView!
    lazy var collectionView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        let width = (UIApplication.shared.windows.first?.bounds.width ?? 300)/2 - 50
        layout.itemSize = CGSize(width: width, height: 200)
        layout.minimumLineSpacing = 10
        layout.scrollDirection = .vertical
        layout.sectionInset = UIEdgeInsets(top: 40, left: 20, bottom: 20, right: 20)
        let collection = UICollectionView(frame: .zero, collectionViewLayout: layout)
        collection.backgroundColor = .white
        collection.layer.cornerRadius = 15
        collection.layer.shadowColor = UIColor.gray.cgColor
        collection.layer.shadowOffset = CGSize(width: 0, height: 1)
        collection.layer.shadowOpacity = 0.5
        collection.layer.shadowRadius = 10
        collection.clipsToBounds = false
        collection.layer.masksToBounds = false
        return collection
    }()
    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var successPaymentView: SucdcessPaymentView!
    let favoriteView = likeView()
    // MARK: - Initializer
    init(viewModel: HomeViewModel) {
        self.viewModel = viewModel
        super.init(nibName: nil, bundle: nil)
    }
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    // MARK: - Cycle
    override func viewWillAppear(_ animated: Bool) {
        navigationController?.setNavigationBarHidden(true, animated: true)
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        layoutView()
        viewModel.set(self)
        viewModel.viewDidLoad()
    }
    //MARK:- View Configuration
    func layoutView() {
        addCollectionView()
        configurationLikeView()
    }
    func addCollectionView() {
        holderCollectionView = BaseCollectionView(collectionView: collectionView)
        holderCollectionView.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(holderCollectionView)
        NSLayoutConstraint.activate([
            holderCollectionView.topAnchor.constraint(equalTo: successPaymentView.bottomAnchor, constant: 30),
            holderCollectionView.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 20),
            holderCollectionView.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -20),
            holderCollectionView.bottomAnchor.constraint(equalTo: view.bottomAnchor)])
    }
    // MARK: - Configuration HeaderView
    func configurationHeaderView(){
        headerView.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(headerView)
        NSLayoutConstraint.activate([headerView.topAnchor.constraint(equalTo: view.topAnchor),
                                     headerView.trailingAnchor.constraint(equalTo: view.trailingAnchor),
                                     headerView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
                                     headerView.heightAnchor.constraint(equalToConstant: 90)])
    }
    // MARK: - Configure Success Payment View
    func configurationSuccessView() {
        successPaymentView.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(successPaymentView)
        NSLayoutConstraint.activate([successPaymentView.topAnchor.constraint(equalTo: headerView.bottomAnchor, constant: 10), successPaymentView.centerXAnchor.constraint(equalTo: view.centerXAnchor)])
    }
    // MARK: - Like View
    func configurationLikeView() {
        favoriteView.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(favoriteView)
        NSLayoutConstraint.activate([favoriteView.centerXAnchor.constraint(equalTo: view.centerXAnchor),
                                     favoriteView.widthAnchor.constraint(equalToConstant: 130),
                                     favoriteView.heightAnchor.constraint(equalToConstant: 30),
                                     favoriteView.topAnchor.constraint(equalTo: successPaymentView.bottomAnchor, constant: 15)])
    }
}
extension HomeViewController: Viewable {
    func show(result: Result<Any, Errors>) {
        switch result {
        case .success(let reusableCells):
            guard let sections = reusableCells as? [Sectionable] else { return }
            configureCollectionView(sections)
        case .failure:
            //TODO:Show error view
            break
        }
    }
    func configureCollectionView(_ sections: [Sectionable]){
        holderCollectionView.loadItems(sections: sections, delegate: self)
    }
}
extension HomeViewController: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let food = viewModel.foods[indexPath.row]
        delegate?.didTapOnCell(with: food)
    }
}
