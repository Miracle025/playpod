//
//  Food.swift
//  PlayPodTechnicalTest
//
//  Created by Fateme on 7/26/1399 AP.
//  Copyright © 1399 AP Fateme. All rights reserved.
//
struct Food: Codable {
    let icon: String
    let price: String
    let title: String
}
