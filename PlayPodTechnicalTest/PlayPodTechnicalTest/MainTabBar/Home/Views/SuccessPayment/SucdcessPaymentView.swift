//
//  SucdcessPaymentView.swift
//  PlayPodTechnicalTest
//
//  Created by Fateme on 7/26/1399 AP.
//  Copyright © 1399 AP Fateme. All rights reserved.
//

import UIKit

class SucdcessPaymentView: UIView {
    override init(frame: CGRect) {
        super.init(frame: frame)
        commit()
    }
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        commit()
    }
    func commit() {
        let nib = UINib(nibName: String(describing: SucdcessPaymentView.self), bundle: nil)
        let views = nib.instantiate(withOwner: self, options: nil)
        guard let view = views.first as? UIView else {return}
        view.frame = bounds
        addSubview(view)
    }
}
