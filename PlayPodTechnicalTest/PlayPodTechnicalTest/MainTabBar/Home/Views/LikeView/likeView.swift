//
//  likeView.swift
//  PlayPodTechnicalTest
//
//  Created by Fateme on 7/26/1399 AP.
//  Copyright © 1399 AP Fateme. All rights reserved.
//

import UIKit
import Foundation
class likeView: UIView {
    var label: UILabel = {
        let label = UILabel()
        label.backgroundColor = .clear
        label.text = "May You Like"
        label.textColor = .white
        label.textAlignment = .center
        label.font = UIFont(name: "", size: 10)
        return label
    }()
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.backgroundColor = Colors.sharedInstance.brown
        self.layer.borderColor = Colors.sharedInstance.darkBrown.cgColor
        self.layer.borderWidth = 1
        self.layer.cornerRadius = 15
        self.addSubview(label)
        configurationLabel()
    }
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
    func configurationLabel() {
        label.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([label.centerYAnchor.constraint(equalTo: self.centerYAnchor),
                                     label.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: -5),
                                     label.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: 5)])
    }
}
