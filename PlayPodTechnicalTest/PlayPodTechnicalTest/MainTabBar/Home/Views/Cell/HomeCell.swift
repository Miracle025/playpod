//
//  HomeCell.swift
//  PlayPodTechnicalTest
//
//  Created by Fateme on 7/25/1399 AP.
//  Copyright © 1399 AP Fateme. All rights reserved.
//

import UIKit

class HomeCell: UICollectionViewCell {
    // MARK: - Views
    @IBOutlet weak var image: UIImageView! {
        didSet {
            image.layer.cornerRadius = 15
            image.layer.masksToBounds = true
        }
    }
    @IBOutlet weak var price: UILabel! {
        didSet {
            
        }
    }
    @IBOutlet weak var name: UILabel! {
        didSet {
            
        }
    }
    @IBOutlet weak var maxConstraintImage: NSLayoutConstraint! {
        didSet {
            maxConstraintImage.constant = (UIApplication.shared.windows.first?.bounds.width ?? 300)/2 - 50
        }
    }
    // MARK: - Properties
    static let reuseID: String = {
        return String(describing: HomeCell.self)
    }()
    // MARK: - awakeFromNib
    override func awakeFromNib() {
        super.awakeFromNib()
        self.backgroundColor = .white
        // Initialization code
    }

}
