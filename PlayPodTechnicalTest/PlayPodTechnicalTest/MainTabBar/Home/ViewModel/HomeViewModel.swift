//
//  HomeViewModel.swift
//  PlayPodTechnicalTest
//
//  Created by Fateme on 7/26/1399 AP.
//  Copyright © 1399 AP Fateme. All rights reserved.
//
import Foundation
class HomeViewModel: ViewModelProviderMappable {
    typealias Model = Food
    var interactor: Interactor<Food>
    weak var view: Viewable?
    var sections = [Sectionable]()
    var foods = [Food]()
    required init(interactor: Interactor<Food>) {
        self.interactor = interactor
    }
    func set(_ view: Viewable) {
        self.view = view
    }
    func viewDidLoad() {
        loadItems()
    }
    func getItems() {
        //TODO:Add mock data
        foods = [Food(icon: "", price: "", title: ""), Food(icon: "", price: "", title: ""), Food(icon: "", price: "", title: "")]
    }
    func loadItems() {
        getItems()
        prepareCells(foods)
        self.view?.show(result: .success(sections))
    }
    func prepareCells(_ items: [Food]) {
        let cells = items.map({ CellViewModel(reuseIdentifier: HomeCell.reuseID, cellClass: HomeCell.self, model: $0)})
        sections = [SectionProvider(cells: cells, headerView: nil, footerView: nil)]
    }
}
