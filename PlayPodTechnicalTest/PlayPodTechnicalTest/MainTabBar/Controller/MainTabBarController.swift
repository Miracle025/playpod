//
//  MainTabBarController.swift
//  PlayPodTechnicalTest
//
//  Created by Fateme on 7/25/1399 AP.
//  Copyright © 1399 AP Fateme. All rights reserved.
//

import UIKit
class MainTabBarController: UITabBarController {
    let viewModel =  MainTabBarViewModel()
    override func viewDidLoad() {
        super.viewDidLoad()
        viewModel.set(self)
        viewModel.viewDidLoad()
    }
}
extension MainTabBarController: Viewable {
    func show(result: Result<Any, Errors>) {
        switch result {
        case .success(let coordinators):
            guard let coordinators = coordinators as? [Coordinator] else { return }
            coordinators.forEach({ $0.start()})
            viewControllers = coordinators.map({$0.navigationController})
            selectedIndex = 1
        case .failure:
            return
        }
    }
}
