//
//  GifViewController.swift
//  PlayPodTechnicalTest
//
//  Created by Fateme on 7/25/1399 AP.
//  Copyright © 1399 AP Fateme. All rights reserved.
//

import UIKit
class GifViewController: UIViewController, CAAnimationDelegate {

    let gifView: UIView = {
       let view = UIView()
        view.backgroundColor = .clear
        return view
    }()
    let label: UILabel = {
        let label = UILabel()
        label.text = "Buy"
        label.textColor = .white
        label.textAlignment = .center
        return label
    }()
    let image: UIImageView = {
        let image = UIImageView()
        image.image = UIImage(named: "buy")
        return image
    }()
    let animLayer = CALayer()
    let cornerRadiusAnim = CABasicAnimation(keyPath: "cornerRadius")
    let cornerRadiusUndoAnim = CABasicAnimation(keyPath: "cornerRadius")
    let widthAnim = CABasicAnimation(keyPath: "bounds.size.width")
    let animDuration = TimeInterval(1.0)
    let layerSize = CGFloat(70)

    override func viewDidLoad() {
        super.viewDidLoad()
        let rect = view.frame
        gifView.frame = CGRect(x: rect.width*0.5-layerSize*0.5, y: rect.height*0.5, width: layerSize, height: layerSize)
        view.addSubview(gifView)
    
        animLayer.backgroundColor = Colors.sharedInstance.mediumBrown.cgColor
        animLayer.frame = CGRect(x: 0, y: 0, width: layerSize, height: layerSize)
        animLayer.cornerRadius = layerSize*0.5;
        animLayer.anchorPoint = CGPoint(x: 0.5, y: 0.5)
        gifView.layer.addSublayer(animLayer)

        label.frame = CGRect(x: 0, y: 0, width: gifView.frame.width, height: gifView.frame.height)
        gifView.addSubview(label)
        label.transform = CGAffineTransform(scaleX: 0.1, y: 0.1)
        
        image.frame = CGRect(x: 10, y: 10, width: gifView.frame.width-20, height: gifView.frame.height-20)
        gifView.addSubview(image)
    
        //decrease width
        widthAnim.duration = animDuration
        widthAnim.fromValue = 110
        widthAnim.toValue = animLayer.frame.size.width
        widthAnim.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeOut)
        
        // increases the width, and autoreverses on completion
        widthAnim.duration = animDuration
        widthAnim.fromValue = animLayer.frame.size.width
        widthAnim.toValue = 110
        widthAnim.autoreverses = true
        widthAnim.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeInEaseOut)

        // timing function to make it look nice
        widthAnim.delegate = self // so that we get notified when the width animation finishes
        
    }
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        widthAnim.beginTime = CACurrentMediaTime()
        
        animLayer.add(widthAnim, forKey: "widthAnim")
        CATransaction.begin()
        CATransaction.setDisableActions(true)
        CATransaction.commit()
        UIView.animate(withDuration: 0.5, animations: {
            self.image.transform = CGAffineTransform(scaleX: 0.1, y: 0.1)
        },completion: { _ in
            UIView.animate(withDuration: 0.5) {
                self.label.transform = CGAffineTransform(scaleX: 1, y: 1)
            }
        })
    }
    func animationDidStop(_ anim: CAAnimation, finished flag: Bool) {
        animLayer.add(widthAnim, forKey: "widthAnim")
        CATransaction.begin()
        CATransaction.setDisableActions(true)
        CATransaction.commit()
    }
}
