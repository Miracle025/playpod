//
//  BlinkViewController.swift
//  PlayPodTechnicalTest
//
//  Created by Fateme on 7/25/1399 AP.
//  Copyright © 1399 AP Fateme. All rights reserved.
//

import UIKit
import MetalKit
class BlinkViewController: UIViewController {
    private var renderer: Renderer?
    let metalKitView = MTKView()
    override func viewDidLoad() {
        super.viewDidLoad()
        view.addSubview(metalKitView)
        metalKitView.translatesAutoresizingMaskIntoConstraints = false
        metalKitView.pinToEdge(view)
        metalKitView.enableSetNeedsDisplay = true
        metalKitView.device = MTLCreateSystemDefaultDevice()
        metalKitView.clearColor = getRandomColor()
        renderer = metalKitView.device.flatMap(Renderer.init)
        metalKitView.delegate = renderer
    }
    func getRandomColor() -> MTLClearColor {
         let red:CGFloat = CGFloat(drand48())
         let green:CGFloat = CGFloat(drand48())
         let blue:CGFloat = CGFloat(drand48())

        return MTLClearColorMake(Double(red), Double(blue), Double(green), 1.0)
    }
}

private final class Renderer: NSObject, MTKViewDelegate {
    private let queue: MTLCommandQueue
    init?(device: MTLDevice) {
        guard let queue = device.makeCommandQueue() else { return nil }
        self.queue = queue
    }
    func mtkView(_ view: MTKView, drawableSizeWillChange size: CGSize) {
        print("drawableSizeWillChange", size)
    }
    
    func draw(in view: MTKView) {
        guard
            let renderPassDescriptor = view.currentRenderPassDescriptor,
            let commandBuffer = queue.makeCommandBuffer(),
            let commandEncoder = commandBuffer.makeRenderCommandEncoder(descriptor: renderPassDescriptor),
            let drawable = view.currentDrawable else {
                return
        }
        commandEncoder.endEncoding()
        commandBuffer.present(drawable)
        commandBuffer.commit()
    }
}

