//
//  MainTabBarViewModel.swift
//  PlayPodTechnicalTest
//
//  Created by Fateme on 7/25/1399 AP.
//  Copyright © 1399 AP Fateme. All rights reserved.
//

import UIKit
class MainTabBarViewModel: ViewModelProvider {
    weak var view: Viewable?
    func set(_ view: Viewable) {
        self.view = view
    }
    func viewDidLoad() {
        let home = HomeCoordiantor(navigationController: CustomNavigationController())
        let gif = GifCoordinator(navigationController: CustomNavigationController())
        let blink = BlinkCoordinator(navigationController: CustomNavigationController())
        let coordinators: [Any] = [gif, home, blink]
        view?.show(result: .success(coordinators))
    }
}
