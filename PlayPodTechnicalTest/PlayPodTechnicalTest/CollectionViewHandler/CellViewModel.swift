//
//  CellViewModel.swift
//  PlayPodTechnicalTest
//
//  Created by Fateme on 7/25/1399 AP.
//  Copyright © 1399 AP Fateme. All rights reserved.
//

import Foundation
class CellViewModel: Reusable {
    let reuseIdentifier: String
    let cellClass: AnyClass
    let model: Any
    required init(reuseIdentifier: String, cellClass: AnyClass, model: Any) {
        self.reuseIdentifier = reuseIdentifier
        self.cellClass = cellClass
        self.model = model
    }
    func getReusIdentifier() -> String { return reuseIdentifier }
    func getCellClass() -> AnyClass { return cellClass }
    func getModel() -> Any { return model }
}
