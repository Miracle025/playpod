//
//  BaseCollectionView.swift
//  PlayPodTechnicalTest
//
//  Created by Fateme on 7/25/1399 AP.
//  Copyright © 1399 AP Fateme. All rights reserved.
//
import UIKit.UICollectionView
class BaseCollectionView: UIView, CollectionViewProvider {
    var collectionView: UICollectionView
    var dataSourceHandler: CollectionViewDataSourceHandler!
    var delegateHandler: CollectionViewDelegateHandler!
    required init(collectionView: UICollectionView) {
        self.collectionView = collectionView
        super.init(frame: .zero)
        addSubview(self.collectionView)
        self.collectionView.pinToEdge(self)
    }
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
