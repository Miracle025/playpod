//
//  CollectionViewDataSourceHandler.swift
//  PlayPodTechnicalTest
//
//  Created by Fateme on 7/25/1399 AP.
//  Copyright © 1399 AP Fateme. All rights reserved.
//

import UIKit.UICollectionView
class CollectionViewDataSourceHandler: NSObject, UICollectionViewDataSource {
    var sections: [Sectionable]
    init(sections: [Sectionable]) {
        self.sections = sections
    }
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return sections.count
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return sections[section].getCells().count
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let item = sections[indexPath.section].getCells()[indexPath.row]
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: item.getReusIdentifier(), for: indexPath)
        (cell as? Updatable)?.update(model: item.getModel())
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        let section = sections[indexPath.section]
        if let headerView = section.getHeaderView() {
            return collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: headerView.reuseID, for: indexPath)
        } else if let footerView = section.getFooterView() {
            return collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: footerView.reuseID, for: indexPath)
        }
        return UICollectionReusableView()
    }
}
