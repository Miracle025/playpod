//
//  CollectionViewDelegateHandler.swift
//  PlayPodTechnicalTest
//
//  Created by Fateme on 7/25/1399 AP.
//  Copyright © 1399 AP Fateme. All rights reserved.
//
import UIKit.UICollectionView
import RxSwift
class CollectionViewDelegateHandler: NSObject, UICollectionViewDelegate{
    let selectedItem = PublishSubject<Any>()
    var sections: [Sectionable]
    init(sections: [Sectionable]) {
        self.sections = sections
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        guard indexPath.section < sections.count && indexPath.row < sections[indexPath.section].getCells().count else { return }
        let model = sections[indexPath.section].getCells()[indexPath.row].getModel()
        selectedItem.onNext(model)
    }
}
